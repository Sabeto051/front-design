import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.buildForm();
  }

  submit(event: Event) {
    event.preventDefault();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      valoracion: ['', [Validators.required]],
      frecuencia: ['', [Validators.required]],
    });
  }
}
