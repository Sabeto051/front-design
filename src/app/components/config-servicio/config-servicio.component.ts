import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-config-servicio',
  templateUrl: './config-servicio.component.html',
  styleUrls: ['./config-servicio.component.scss'],
})
export class ConfigServicioComponent implements OnInit {
  form: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.buildForm();
  }

  ngOnInit(): void {
    // this.form.valueChanges.subscribe((x) => {
    //   console.log(x);
    // });
  }

  submit(event: Event) {
    event.preventDefault();
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      valoracion: ['', [Validators.required]],
      frecuencia: ['', [Validators.required]],
    });
  }
}
