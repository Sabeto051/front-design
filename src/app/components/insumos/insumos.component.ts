import { Component, OnInit } from '@angular/core';
import * as Handsontable from 'handsontable';

const DATA_IMPACTO = [
  { rango: 1, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 2, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 3, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 4, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 5, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 6, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 7, limite_inferior: 0, limite_superior: 0, prob: 0 },
];

const DATA_HORAS = [
  { rango: 1, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 2, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 3, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 4, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 5, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 6, limite_inferior: 0, limite_superior: 0, prob: 0 },
  { rango: 7, limite_inferior: 0, limite_superior: 0, prob: 0 },
];

@Component({
  selector: 'app-insumos',
  templateUrl: './insumos.component.html',
  styleUrls: ['./insumos.component.scss'],
})
export class InsumosComponent implements OnInit {
  dataSource = DATA_IMPACTO;
  dataSource2 = DATA_HORAS;

  constructor() {}

  ngOnInit(): void {}
}
