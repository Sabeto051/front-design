import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigServicioComponent } from './components/config-servicio/config-servicio.component';
import { InsumosComponent } from './components/insumos/insumos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DomseguroPipe } from './pipes/domseguro.pipe';

// Material
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCommonModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule } from '@angular/material/stepper';
import { MatButtonModule } from '@angular/material/button';

// Handsometable
import { HotTableModule } from '@handsontable/angular';

@NgModule({
  declarations: [
    AppComponent,
    ConfigServicioComponent,
    InsumosComponent,
    DomseguroPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    MatCommonModule,
    MatFormFieldModule,
    MatSelectModule,
    MatTableModule,
    MatInputModule,
    MatStepperModule,
    MatButtonModule,
    HotTableModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
